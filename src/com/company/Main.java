package com.company;
import java.util.*;
public class Main {

    public static void main(String[] args) {

        System.out.println("1.----List----");
        List Animals = new ArrayList();
        Animals.add("Dog");
        Animals.add("Goat");
        Animals.add("Snake");
        Animals.add("Pig");
        Animals.add("Dock");
        Animals.add("Snail");

        for (Object str : Animals ){
            System.out.println((String) str);
        }
        System.out.println("Above we see a list is organized, and values are retrieved according to where they were palaced\n" +
                "for instance if we print this list as many times as we please the list will always have Dog at the \n" +
                "beginning and all other values will not experience change in their position.");


        System.out.println("\n\n2.--Set...a.TreeSet..b.Generic implimentation------");
        TreeSet<String> StudentSet = new TreeSet<String>(); //Generic implimentation
        StudentSet.add("Austin");
        StudentSet.add("Zaynab");
        StudentSet.add("Austin");
        StudentSet.add("zion");
        StudentSet.add("Vivian");

        System.out.println("Original Set:" + StudentSet);
        System.out.println("First Name: "+ StudentSet.first());
        System.out.println("Last Name: "+ StudentSet.last());
        System.out.println("Here we see two features of a set. It organizes the information alphabetically\n" +
                "it doesnt not allow duplication of values. You also observe that uppercase values have precedence than\n" +
                "lower case values, as Zaynab coming before zion\n\n");


        System.out.println("3.--Queue--");
        Queue queue = new PriorityQueue();
        queue.add("Hi");
        queue.add("Austin");
        queue.add("name");
        queue.add("is");
        queue.add("Austin");
        queue.add("hi");
        System.out.println(queue);
        System.out.println("As is obvious in the code implimentation of the queue, we can tell\n" +
                "that duplication and sorting takes place\n\n");


        System.out.println("5.---Using comparator on a list collection---");
        List<Integer> stringList = new ArrayList<>();
        stringList.add(11);
        stringList.add(2);
        stringList.add(5);
        stringList.add(1);
        stringList.add(4);

        System.out.println("Before sort: " + stringList);
        Collections.sort(stringList);
        System.out.println("After sort: " + stringList);









    }

}
